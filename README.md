# Alpine-base

## Description

Docker alpine base image with custom user, user management and entrypoint scripts.

[Source](https://gitlab.com/dotslashme/docker-alpine-base/) | [Issues](https://gitlab.com/dotslashme/docker-alpine-base/-/issues) | [Vulnerabilities](https://gitlab.com/dotslashme/docker-alpine-base/-/security/vulnerability_report)

### Versioning

The version tag always follow alpine versioning, meaning the tag `:3` of this image builds upon `alpine:3`.

## Binaries

The following binaries are installed in addition to what comes with the `alpine:3` image:

- **shadow** - needed to be able to create/change users
- **su-exec** - needed to be able to execute binaries as another user


## Features

- Custom user (dockrun) with customizable `UID:GID`.
- Provides a custom entrypoint at `/docker-entrypoint.sh` that will run `CMD`s as the dockrun user.

### Custom user

The Alpine base image features a custom user and group to run `CMD`s with. It is set up in the Docker file during the
build phase and both the user and group are named `dockrun`. Unless overridden, their UID and GID is set to `1000`.

#### Motivation for a custom user

The motivation for a dedicated user has absolutely nothing to do with security, if anything, using a different user will
most likely introduce security issues, rather than solve them. The purpose of the dedicated user is to allow a custom
UID/GID mapping to bare metal users, allowing primarily bind mounts to work seamlessly with directories owned by users
other than root.  
Execution of the `CMD` with the custom user is accomplished using the `su-exec` binary.

#### Custom UID and GID

Sometime you need to run with a custom UID and GID. This image supports environment variables for just this case:

- PUID
- PGID

Every image based upon this base will automatically support these as well and in order to use them, you either supply
the environment variables through the `docker run` command or in your `docker-compose.yml`.

**Example:** `docker run -it -e PGID=995 dotslashme/alpine-base:3 sh`

The UID:GID change is accomplished through the [docker-entrypoint.sh](https://gitlab.com/dotslashme/docker-alpine-base/-/blob/main/docker-entrypoint.sh) script.

### Custom entrypoint

A custom entrypoint has been installed in `/docker-entrypoint.sh`, which will run any `CMD` you define in your own
Dockerfile, with the user `dockrun`.
